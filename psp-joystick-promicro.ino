#include  <Keyboard.h>
#include  <Mouse.h>

const byte lrPin = A3;
const byte udPin = A2;
const byte ledPin = LED_BUILTIN;

void setup() {
  pinMode(ledPin, OUTPUT);
  pinMode(lrPin, INPUT);
  pinMode(udPin, INPUT);
  Serial.begin(9600);
}

void loop() {
  int udVal = analogRead(udPin);
  int lrVal = analogRead(lrPin);
  Serial.write("\nup/down: ");
  Serial.print(udVal, DEC);
  Serial.write("; left/right: ");
  Serial.print(lrVal);
  if (udVal > 700) {
    Keyboard.write('a');
  } else if (udVal < 200) {
    Mouse.move(10, 0, 0);
  }
  delay(333);
}


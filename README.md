# Getting set up
https://arduino.github.io/arduino-cli/0.28/getting-started/
https://www.sparkfun.com/tutorials/337
https://www.arduino.cc/reference/en/language/functions/usb/keyboard/

## Install `arduino-cli`
```bash
yay -S arduino-cli
arduino-cli lib install Keyboard
arduino-cli lib install Mouse
```

## Add user to group
Your user needs to be able to write to the serial interface for the promicro.
```bash
sudo usermod -aG uucp $USER
```

Then log out/in or reload the groups with
```bash
exec su -l $USER
```

## Install core
```bash
arduino-cli core install arduino:avr
arduino-cli core list  # to check it's there
```

## Enable bootloader
Press the reset button on the promicro. If you have QMK loaded, it'll only stay
in bootloader mode for 8 seconds, so do the next step quickly. Once a basic
sketch is loaded, the bootloader always seems to show up.

## Check board is connected
```bash
$ arduino-cli board list
Port         Protocol Type              Board Name       FQBN                 Core
/dev/ttyACM0 serial   Serial Port (USB) Arduino Leonardo arduino:avr:leonardo arduino:avr
```
